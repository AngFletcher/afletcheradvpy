# AdPyFinal
Abstract: Our project will be a web-scraping tool that will scrape 5 popular textbook retail 
websites for the cheapest price of a given textbook. The program will return the cheapest available 
options given the name, author, and edition of a textbook for both rental and purchase options.
This project would be useful to students looking for the best deal on textbooks.
