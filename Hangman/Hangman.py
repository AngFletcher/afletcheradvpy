#!/usr/bin/env python3

import random
import HangmanFunctions as hf
import HangmanPlayFunctions as hpf


# starting variables
streak = 0 #consecutive rounds won by player
level = 1 # current level of player
wins = 0 # total wins in a level
words = [] # word bank list
usedWords = [] # list of words that have already been used
usedWords = [False] * 100
score = 0
play = True


#Introduction
print("Welcome to Hangman!")
player = input("Enter your name: ")
start = input(f"""Hi {player}, You can enter 'help' at any time to view a help menu, 'score' to see the leader board or 'quit' to end the program.
Press any key to continue.""")
print("Lets get started!\n")


# Read word bank into list
words = hpf.readFile()


# Game Session loop
while play:
    
    # reset value of counters on each new round
    count = 1
    move = 0
    guessList = []

    
    # Select random word based on the level the player is on
    randWord = hpf.randWordGen(level, words, usedWords)

    # create mask of guess word and print the starting screen
    mask = hpf.maskWord(randWord)
    print("")
    print(mask)
    print("")
    hpf.board(move)


    # Letter guess loop
    while move < 6 and mask.replace(" ","") != randWord.replace(" ","") and play == True:
        
        # Get guess from user
        guess = hpf.guessIn(guessList)

        #Menu options
        if guess == "HELP":
            hf.gameHelp()
        elif guess == "SCORE":
            hf.printScores()
        elif guess == "QUIT":
            print( "Goodbye")
            play = False
            playAgain = 'n'
            
        # Word guess
        elif len(guess) == len(randWord):
            if guess.replace(" ","") == randWord.replace(" ",""):
                mask = ' '.join(randWord)
                print(mask)
                if len(guessList) < 4:
                    score = score + 500 * level
                elif len(guessList) < 6:
                    score = score + 350 * level
                else:
                    score = score + 100 * level
            else:
                print("\nGood try but that is not the correct answer.\n")
                score = score - 50*level
        elif len(guess) > len(randWord) or (len(guess) < len(randWord) and len(guess) >= 3):
            print("\nBetter double check that letter count!\n")
            
        # Valid guess
        elif guess[0] in randWord:
            for i in range(0, len(randWord)):
                if guess[0] == randWord[i]:
                    mask = mask[:i+i] + guess[0] + " " + mask[i+i+2:]                
                    if count <= 5:
                        score = score + 10 * count*level
                    else:
                        score = score + 50 * level
            print("\nYes. There is atleast one " + guess[0] + " in the word\n")
            count = count + 1
            guessList.extend(guess[0])
            
        # Wrong guess
        else:
            move = move +1
            count = 1
            guessList.extend(guess[0])
            
        # Print Hangman Screen
        if play:
            hpf.reprint(mask, move, guessList, score)


    # If user guesses word
    if mask.replace(" ","") == randWord.replace(" ",""):
        print("Correct, the word was: " + randWord.upper() + "\n")
        streak = streak + 1
        wins = wins + 1
        if wins == 5:
            level = level + 1
            wins = 0
        playAgain = hf.rematch(1, score, level, streak, player)
    elif play != False:
        print("Sorry, the word was: " + randWord.upper() + "\n")
        playAgain = hf.rematch(0, score, level, streak, player)
        streak = 0
        wins = 0
        level = 1
        score = 0

    
    if playAgain == 'y':
        play = True
    else:
        play = False
