#Hangman

##Introduction
This is a python command line version of the popular pen and paper game Hangman. At the start of the game a word to guess is represented as a string of dashes and the gallows are empty. The user inputs individual letters to try and guess the word. For each incorrect guess a part of the hangman appears. The object of the game is to uncover the word before the full Hangman is build. This version of Hangman tallies a score for the user based on the number of correct guesses and the level the player is on. Concecutive correct answers increase the score value. As an added option for this version, the user can also guess the entire word at any point during the game and as many times as they want but each incorrect answer results in a points deduction. There are four levels in the game. The point values increase with each level as well as the difficulty of the guess words. 
![Winner](https://bitbucket.org/AngFletcher/afletcheradvpy/src/master/Hangman/ReadMeData/Winner.JPG)
![Loser](https://bitbucket.org/AngFletcher/afletcheradvpy/src/master/Hangman/ReadMeData/loser.JPG)

## Prerequistes
This program is designed to run with the most current version of Python 3.

##Installing
�	This is a private repository and requires the Authors permission to be accessed. Audiences with access have already ben given access to the repository and may install it by cloning the project onto thier system using the clone resource found on bitbucket.org

## Deployment
The program will begin by asking for the users name. Once a name is entered a short introduction will appear which includes the keywords needed to acess features such as the help menu and top scores list. 
![StartScreen](https://bitbucket.org/AngFletcher/afletcheradvpy/src/master/Hangman/ReadMeData/StartScreen.JPG)

After the player acknowledges the screen the game play begins.
![GuessScreen](https://bitbucket.org/AngFletcher/afletcheradvpy/src/master/Hangman/ReadMeData/GuessScreen.JPG)
![WordGuessScreen](https://bitbucket.org/AngFletcher/afletcheradvpy/src/master/Hangman/ReadMeData/WordGuess.JPG)

## Authors

�	Angeline Fletcher - Initial work


## Acknowledgments

�	This program does not utilize any external libraries or features

## Resources and Outside help
�	https://www.digitalocean.com/community/tutorials/how-to-use-string-formatters-in-python-3
