#!/usr/bin/env python3

import csv

# determine if the user wants to play again
def rematch(w, s, l, k, player):
    if w == 1:
        game = " YOU WON "
    else:
        game = "GAME OVER"
    print(f"""
    * * * * * * * * *
        {game}    
       Score:   {s}  
       Level:   {l}    
       Streak:  {k}    
    * * * * * * * * *
    """)
    done = False
    while done == False:
        match=input("Would you like to play again (y)es or (n)o? ")
        if match == 'help':
            gameHelp()
            done = False
        elif match == 'score':
            printScores()
            done = False
        elif match == 'quit':
            print("Goodbye")
            return 'n'
        elif match == 'n':
            updateScores(player, s, l, k)
            done = True
        else:
            done = True
    return match.lower()

# Update the high score list and save it to the file
def updateScores(player, score, level, streak):
    player = player.upper()
    hScores = []
    with open ('HighScores.txt') as file:
        hScore = file.read().splitlines()
        hScores = [x.strip().split(",") for x in hScore]
    hScores = [x for x in hScores if x != ['']]
    i = 0
    if score > int(hScores[19][1]):
        while score <= int(hScores[i][1]) and i <= 19:
            i = i + 1
        hScores.insert(i, [player, score, level, streak])
        hScores = hScores[:-1]
        with open('HighScores.txt', 'w') as out:
            write = csv.writer(out, dialect='excel')
            write.writerows(hScores)
    printScores()

# Read the top scores from the file and print them to the screen            
def printScores():
    
    with open ('HighScores.txt') as file:
        hScore = file.read().splitlines()
        hScores = [x.strip().split(",") for x in hScore]
    hScores = [x for x in hScores if x != ['']]

    # Formating information found at: https://www.digitalocean.com/community/tutorials/how-to-use-string-formatters-in-python-3

    print("\n\n")
    print("        {:12} {:6} {:5} {:5}".format("PLAYER", "SCORE", "LEVEL", "STREAK"))
    for i in range(0,20):
        if hScores[i] != '':
            print("        {:12} {:8} {:5} {:5}".format(hScores[i][0], hScores[i][1], hScores[i][2], hScores[i][3]))    
    input("\n\nPress enter to continue")

# Function to display playing instructions to the user
def gameHelp():
    print("""
Hangman Playing Instructions:

HOW TO PLAY: A player starts on level 0 with a score of 0. The game will
begin by displaying a number of dashes where each dash cooresponds to a
letter in the word or phrase to be guessed. A picture of an empty gallow
will also appear. To play input a letter and press enter. If the letter
is in the word the dash or dashes with be replaced with that letter and
the letter bank and score will be updated (See Scoring). If the letter is
not contained in the word the letter bank will be updated and a portion of
the hangman will be displayed on the gallows. The player can guess the
entire word at any time by inputing the word or phrase into the command line.
Play will continue until the word is uncovered or until the complete hangman
appears and game over.

SCORING: Level 1: Correct answers recieve 10 points for each letter in
the word. Consecutive guesses are incremented by 10 points up to 50 points max.
Level 2: Correct answer recieves 20 points, incremented by 20 up to 100 points
Level 3: Correct answer recieves 30 points, incremented by 30 up to 150 points
Level 4: Correct answer recieves 40 points, incremented by 40 up to 200 points
The score carries through each level until the player reaches game over or
chooses not to continue.

Players recieve a bonus for guessing the word within a short number of terms.
The bouns is determined by the number of guesses already used and the level the
player is on. 

LEVELS: Player will level up after successfully uncovering five consecutive
words. New more difficult words will be introduced with each new level.

HIGH SCORE: The top 20 scores can be accessed by typing "score" in the command
line at any time.

Quit: Using the 'quit' command automatically ends the program. Any any high
scores will not be saved when using this option.
""")
    input("Press any key to continue")
