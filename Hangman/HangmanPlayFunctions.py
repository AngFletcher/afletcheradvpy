#! /usr/bin/env python3

import random

# hangman boards
def board(num):
    if num==0:
        print("""     |==========|
     |          |
                |
                |
                |
                |
                |
                |
                |
                |
           ==========""")
    elif num==1:
        print("""     |==========|
     |          |
    (")         |
                |
                |
                |
                |
                |
                |
                |
           ==========""")
    elif num==2:
        print("""     |==========|
     |          |
    (")         |
    [:]         |
                |
                |
                |
                |
                |
                |
           ==========""")
    elif num==3:
        print("""     |==========|
     |          |
    (")         |
   /[:]         |
                |
                |
                |
                |
                |
                |
           ==========""")
    elif num==4:
        print("""     |==========|
     |          |
    (")         |
   /[:]\        |
                |
                |
                |
                |
                |
                |
           ==========""")
    elif num==5:
        print("""     |==========|
     |          |
    (")         |
   /[:]\        |
   _/           |
                |
                |
                |
                |
                |
           ==========""")
    elif num==6:
        print("""     |==========|
     |          |
    (")         |
   /[:]\        |
   _/ \_        |
                |
                |
                |
                |
                |
           ==========""")


# Read the wordBank from the file and save it to a list
def readFile():
    words = []
    with open('wordBank.txt') as file:
        words = file.read().splitlines()
    return(words)


# Determine a random word in the current level to be used
def randWordGen(level, words, usedWords):
    duplicate = True
    while duplicate:
        randNum = random.randint(0,24)
        randNum = randNum + ((level-1) * 25)
        if usedWords[randNum] == False:
            usedWords[randNum] = True
            duplicate = False    
            randWord = words[randNum]
            randWord = randWord.upper()
    return randWord


# Create a starting mask of the current guess word
def maskWord(word):
    mask = ""
    for i in range(0, len(word)):
        if word[i] == " ":
            mask = mask + "  "
        else:
            mask = mask + "_ "
    return mask


# Get guess from user and determine validity
def guessIn(guessList):
    invalid = True
    while invalid:
        guess = input("Enter a letter: ")
        guess = guess.upper()
	
        # Invalid guesses
        if guess =="" or guess.replace(' ','').isalpha()==False: # method found at https://stackoverflow.com/questions/20890618/isalpha-python-function-wont-consider-spaces
            print("Invalid Input\n")
	
        # duplicate guess
        elif len(guess) == 1 and guess in guessList:
            print("That letter has already been guessed.\n")
            
        else:
            invalid = False
    return guess

 
# Print Hangman Screen	
def reprint(mask, move, guessList, score):
    print("")
    print(mask)
    print("")
    board(move)      
    print("Letters guessed:", end =" ")
    print(', '.join(guessList))
    print(f"Score: {score}")
            
