#!usr/bin/env python

import sys

def answer(sets):
    #for i in range(0, len(sets)):
    print(abs(int(sets[0]) - int(sets[1])))

def solve():
    for line in sys.stdin.readlines():
        sets = line.split()
        answer(sets)
    
def test():
    assert answer(22, 4) == "18"
    print("Passed Test Case 1")
    assert answer(4, 22) == "18"
    print("Passed Test Case 2")
    assert answer(0, 999) == "999"
    print("Passed Test Case 3")
    assert answer(2345, 1234) == "1111"
    print("Passed Test Case 4")

if __name__=="__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        solve()
