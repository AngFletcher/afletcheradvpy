#!usr/bin/env python3

import sys
def answer(question, guessNum, highNum, lowNum, correct):
    if question == "higher":
        lowNum = guessNum
        highNum = highNum
    elif question == "lower":
        highNum = guessNum
        lowNum = lowNum
    elif question == "correct":
        correct = True
    return (int(round(highNum + lowNum) / 2), highNum, lowNum, correct)

def solve():
    lowNum = 0
    highNum = 1001
    guessNum = 500
    correct = False
    counter = 0

    while not correct and counter <= 10:
        print(guessNum, flush = True)
        counter += 1
        question = input()
        guessNum, highNum, lowNum, correct = answer(question, guessNum, highNum, lowNum, correct)

def test():
    assert answer("higher", 500, 1000, 1, False) == 750
    print("Passed Test Case 1")
    assert answer("lower", 225, 250, 200, False) == 212
    print("Passed Test Case 2")
    assert answer("higher", 750, 1000, 500, False) == 825
    print("Passed Test Case 3")
    assert answer("correct", 125, 250, 1, False) == 125
    print("Passed Test Case 4")

if __name__=="__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        solve()
