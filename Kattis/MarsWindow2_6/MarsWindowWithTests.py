#!usr/bin/env python3

import sys
import math

def answer(inYear):
    window = 4
    time = ((int(inYear) - 2018) * 12)
    while window < time:
        window = window + 26
    if window > 12:
        if (window%12 == 0):
            checkYear = ( window / 12 ) + 2017 # December
        else:
            checkYear = (window / 12) + 2018
            checkYear = math.floor( checkYear )
    else:
        checkYear = inYear
    if int(inYear) == int(checkYear):
        return "yes"
    else:
        return "no"

def solve():
    data = sys.stdin.readline()
    inYear = int(data)
    print(answer(inYear))
    
def test():
    assert answer(2018) == "yes"
    print("Passed Test Case 1")
    assert answer(2026) == "yes"
    print("Passed Test Case 2")
    assert answer(2027) == "no"
    print("Passed Test Case 3")
    assert answer(2029) == "yes"
    print("Passed Test Case 4")
    
if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        solve()
