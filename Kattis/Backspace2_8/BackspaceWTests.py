#!usr/bin/env python3

import sys
import math

def answer(data):
    if len(data) > 1:
        i = 0
        fixed = ""
        point = 0
        while i < len(data):
            if data [i] == "<":
                point += 1
            else:
                if point > 0:
                    point -= 1
                else:
                    fixed = fixed + data[i]
            i += 1
        return fixed[::-1]
    else:
        return""
def solve():
    data = sys.stdin.readline().strip()
    print(answer(data[::-1]))

def test():
    assert answer("progrr<ammiii<<n.<g") == "programming"
    print("Passed Test Case 1")
    assert answer("k<Kattoi<<is") == "Kattis"
    print("Passed Test Case 2")
    assert answer("k<asdf<<<<dd<<a") == "a"
    print("Passed Test Case 3")
    assert answer("pp<") == "p"
    print("Passed Test Case 4")

if __name__=="__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        solve()
