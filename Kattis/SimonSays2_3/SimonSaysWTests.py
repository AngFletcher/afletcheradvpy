#!usr/bin/env python3

import sys
import math

def answer(data):
    if data[:10] == "simon says":
        return data[11:]
    else:
        return "\n"
def solve():
    size = int(input())
    while size > 0:
        size -= 1
        data = sys.stdin.readline()
        sys.stdout.write(answer(data))
def test():
    assert answer("""5
simon says write a test
check the code
simon shouts wrong
simon says try again
simon say nope""") == """write a test


try again
"""
    print("Passed Test Case 1")
    assert answer("""2
simon say more kattis fun
ssimon says yes""") == """
"""
    print("Passed Test Case 2")

if __name__=="__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        solve()
