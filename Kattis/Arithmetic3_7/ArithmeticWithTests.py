#!usr/bin/env python3

import sys
import math

def answer(base8):
    base10 = int(base8, 8)
            
    base16 = hex(base10)
    return base16.upper()[2:]

def solve():
    base8 = input()
    if base8 > "0":
        base16 = answer(base8)
    else:
        base16 = "0"
    print(base16)

def test():
    assert answer(4444) == 924
    print("Passed Test Case 1")
    assert answer(20) == 10
    print("Passed Test Case 2")
    assert answer(3211) == 689
    print("Passed Test Case 3")
    assert answer(7654321001234567) == "FAC688053977"
    print("Passed Test Case 4")

if __name__=="__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        solve()
